﻿// Created By : Buddhima Kudagama
// Created On : 2017-02-26
// Description : Login Class : No 9 UI

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace MDIApp.View
{
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        private async void btnLogin_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new MainMenuTabbedPage());
            }
            catch (Exception ex)
            {
               await  DisplayAlert("Error",ex.Message,"OK");
            }
        }

        private void btnForgotPassword_Clicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new ConformPasswordPage());
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }
        }
    }
}

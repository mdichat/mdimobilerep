﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using System.Threading;

namespace MDIApp.View
{
    public partial class VerificationModeSelectionPage : ContentPage
    {
        public VerificationModeSelectionPage()
        {           
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private async void btnVerifyPhone_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool result = await DisplayAlert("Conform", string.Format("Please Conform {0}{1} is your actual phone number.", "+94", "0713775822"), "OK", "Edit");
                if (result)
                {
                  //  actSendingCode.IsVisible = true;
                  //  actSendingCode.IsRunning = true;
                    await Navigation.PushAsync(new VerificationCodeVerify());
                  //  actSendingCode.IsRunning = false;
                   // actSendingCode.IsVisible = false;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private async void btnVerifyEmail_Clicked(object sender, EventArgs e)
        {
            try
            {
                bool result = await DisplayAlert("Conform", string.Format("Please Conform {0} is your actual email.", "kgbuddhima@gmail.com"), "OK", "Edit");
                if (result)
                {
                    await Navigation.PushAsync(new VerificationCodeVerify());
                }
            }
            catch (Exception ex)
            {

                throw;
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace MDIApp.View
{

    public partial class VerificationCodeVerify : ContentPage
    {
        public VerificationCodeVerify()
        {
            
            InitializeComponent();
            this.Title = "Enter Code";
        }

        private void btnVerify_Clicked(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new RegistrationPage());
            }
            catch (Exception ex)
            {
                DisplayAlert("Error", ex.Message, "OK");
            }
        }
    }

}

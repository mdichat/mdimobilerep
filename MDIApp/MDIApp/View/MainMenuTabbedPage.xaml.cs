﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;


namespace MDIApp.View
{
    // [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainMenuTabbedPage : TabbedPage
    {
        public MainMenuTabbedPage()
        {
            InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
        }

        private void lblUpdatePassword_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new ChangePasswordPage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblPermissionCode_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new PermissionCodeOnOffPage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblMessageLifeTime_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new MessageLifetimePage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblAbout_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new AboutPage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblLegalInfo_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new LegalInfoPage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblHelpAndSupport_Tapped(object sender, EventArgs e)
        {
            try
            {
                Navigation.PushAsync(new HelpAndSupportPage());
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void lblrateMDiChat_Tapped(object sender, EventArgs e)
        {

        }

        private void lblLogout_Tapped(object sender, EventArgs e)
        {

        }
    }
}
